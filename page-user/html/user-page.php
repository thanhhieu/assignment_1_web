<?php
    session_start();
    
    
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
    if (!isset($_SESSION['email']) && !isset($_SESSION['temp_email'])){
        header("location:../../login/signin-register-forgotpassword/index.php");
    }
    include("action.php");
$post = new Post();

// if (isset($_POST['post'])) { // Creates instance of Post class when post button is triggered

//     console_log(2);

//     $post->submitPost($_POST['post_text']);
// }
// if (isset($_POST['comment'])) { // Creates instance of Post class when post button is triggered
//     console_log(1);

//     $post->submitCommend();
// }
$page = $post->getIndexPost(($_SESSION['id_temp_user']));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../homepage-main/css/homepage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/page-user.css">
    <!-- <script src="../js/homepage.js"></script> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
<?php
// echo 1;
// echo $_SESSION['admin1'];
?>      
    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../homepage-main/html/homepage.php">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </div>

    <div class="banner">
    	<div class="img-cover">
    		
    	</div>
    	<div class="img-user">
    		<div class="avatar">
                <img src="https://khoso.vn/img/avatar.jpg" alt="">  
            </div>
            <h4><?php 
                    echo mysqli_fetch_array($name)['name']; ?>
            </h4>
            <?php
                if(isset($_SESSION['temp_email']) && isset($_SESSION['email'])){
                if ($_SESSION['email'] != $_SESSION['temp_email']){
                    $is_follow =mysqli_query($con,"select * from user_friend where email_user= '$email' and email_friend = '$temp_email'");
                    if(mysqli_num_rows($is_follow) == 0){
            ?>
                        <button id="btn-fl" type="button" class="btn btn-outline-secondary btn-sm">Follow</button>
            <?php
                    }else{
            ?>
                        <button id="btn-fl" type="button" class="btn btn-success btn-sm">Follow</button>
            <?php
                    }
                }
            }
            ?>
            <?php
                if(isset($_SESSION['admin'])){
                    if($_SESSION['admin'] != $_SESSION['temp_email']){
            ?>
            <button type="button" class="btn btn-danger btn-sm">Xóa</button>
            <?php
                }}
            ?>
    	</div>
    </div>	
    
    <div class="nav-user">
    	<ul class="nav nav-tabs">
		  <li class="nav-item active">
		    <a class="nav-link" href="#">Dòng thời gian</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="introduce.php">Giới thiệu</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="listfriend.php">Bạn bè</a>
		  </li>
		</ul>
    </div>	
        <div class="main-page">
        <div class="container" style="max-width: 1300px;">
            <div class="row">
                <div class="col-left">
                    
                </div>

                <div class="col-main">

                <?php
                    if(isset($_SESSION['email'])){
                        if($_SESSION['email'] == $_SESSION['temp_email']){
                    
                ?>
                <div class="status">
                    <div class="nav-status">
                        <a href="#" class="name12">Chia sẻ kinh nghiệm</a>
                        <a href="#" class="search-button"><i class="fa fa-search pl-3 border-left"></i></a>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" data-toggle="modal" data-target="#exampleModal" placeholder="Chia sẻ trải nghiệm của bản thân với gia đình và bạn bè của bạn!"></textarea>

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="height: auto;">
                                        <div class="nav-status" style="background-color: white;margin: 0;height: 100%;">
                                            <a href="#" class="name12">Chia sẻ kinh nghiệm</a>
                                        </div>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <a href="#">
                                            <div class="load-img">
                                                <label for="file-input">
                                                    <i class="fa fa-arrows"></i>Click vào để tải ảnh lên

                                                </label>

                                                <input onchange="change(event)" style=" display: none;" accept="image/*" id="file-input" type="file" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="text" placeholder="Tiêu đề bài viết" name="title-status" id="title-status" class='title-status'>
                                    </div>
                                    <div class="modal-footer">
                                        <textarea name="post_text" rows="4" cols="50" placeholder="Chia sẻ kinh nghiệm của bạn" id="content-status" class='content-status'></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <img src="../public/emotion.svg" alt="emotion" style="width: 20px; height: 20px;" type="button">
                                        <img src="../public/usertag.svg" alt="usertah" style="width: 20px; height: 20px;" type="button">
                                        <img src="../public/location.svg" alt="location" style="width: 20px; height: 20px;" type="button">
                                        <img src="../public/img.svg" alt="image" style="width: 20px; height: 20px;margin-right: 170px" type="button">
                                     
                                        <button type="submit" name="post" id="post_button" class="btn btn-primary">Chia sẻ</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end modal -->
                    </div>
                </div>
                <?php
                    }}
                ?>
                <!-- Post status -->
                <?php
                foreach ($page as &$row) {               // output data of each row
                    $text = "'";
                    echo      '<div class="content-post">';
                    if ($row['picture']) {
                        echo '<div class="img-post">';
                        echo '<img src="data:image/png;base64,' . $row['picture'] . '" alt="">';
                        echo        '</div>';
                    }
                    echo        '<div class="info-post">';
                    echo            '<a href="#"><img src="../public/avt.png" alt=""></a>';
                    echo            '<span>';
                    echo                '<span><a href="../../database/profile_user.php?id='. $row['id_user'] .'"><b>' . $row['name'] . '</b></a>';
                    if(isset($_SESSION['email'])){
                        if($_SESSION['id_user'] == $row['id_user']){
                            echo '<img type="button" onClick="deletepost('. $text . $row["id"] .  $text .')" src="../public/3dot.svg" alt="" style="float: right;width: 20px;height:auto;">';
                        }
                    }
                    echo '<br></span>';
                    echo                '<span><a href="#">';
                    echo                       ' <p style="color: gray;font-size: 12px;"> Đăng vào '.$row['post_time'].'</p>';
                    echo                    "</a></span>";
                    echo           ' </span>';
                    echo       ' </div>';
                    echo        '<div class="body-post">';
                    echo        nl2br($row["title"]);
                    echo '</br>';
                    echo        nl2br($row["content"]);

                    echo            '<a href="#" data-toggle="modal" data-target="#exampleModal1" style="margin-left:10px;">xem
                            thêm</a>';

                    echo           ' <!-- Modal moreview -->';

                    echo            '<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">';
                    echo                '<div class="modal-dialog" role="document">';
                    echo                    '<div class="modal-content-post" style="background-color: white;">';
                    echo                        '<div class="modal-body-post">';
                    echo                            '<h4><b>' . $row['title'] . '</b></h4>';
                    echo                        '</div>';
                    echo                       ' <div class="modal-header-post" style="padding: 0;">';
                    echo '<img src="data:image/png;base64,' . $row['picture'] . '" alt="">';
                    echo                        '</div>';
                    echo       '<p style="text-align:left;">'. nl2br($row["content"]).'</p>';

                    echo                             '</div>                              ';

                    echo                 '   </div> </div></div>';




                    echo       ' <div class="number-like-cmt">';
                    echo           '<span>
                            <a href="#">
                            <p id="like'.$row['id'].'" style="font-size: 12px;margin-bottom: 0;margin-left: 10px;">' . $row["count_like"] . " người đã thích bài viết này" . '</p>
                            </a></span>
                    </div>
                    <hr style="margin-bottom: 5px;margin-top: 5px;">
                    <div class="icon-like-share">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                <button onClick= "like('  . $text . $row["id"] .  $text . ','. $text . $row["count_like"] .  $text .')" type="button"><img src="../public/like.svg" style="margin: 0;padding: 0;height: 20px;width: 20px;"></button>
                                <button type="button"><img src="../public/comment.svg" style="margin-left: 10px;padding: 0;height: 20px;width: 20px;"></button>
                                    <button type="button"><img src="../public/share.svg" style="margin-left: 10px;padding: 0;height: 20px;width: 20px;"></button>
                                </div>
                                <div class="col-sm">
                                </div>
                                <div class="col-sm" style="text-align: right;">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="../public/3dot.svg" alt="down">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Báo cáo bài viết</a>
                                            <a class="dropdown-item" href="#">Tắt thông báo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin-bottom: 5px;margin-top: 5px;">';
                    if (count($row['listcomment']) > 3) {
                        $numberHide = count($row['listcomment']) - 3;
                        echo '<div class="hide-cmt">
                        <a  id="box1' . $row["id"] . '"  href="javascript:void(0); id="show' . $row["id"] . '" onClick="hide111('  . $text . $row["id"] .  $text . ')">
                            <p style="margin-left: 10px;margin-bottom: 0;font-size: 15px">Xem ' . $numberHide . ' bình luận bị ẩn</p>
                        </a>
                    </div>';
                        echo          ' 
                     <!-- comments -->
                     <div class="comments">
                     
                        
                     <div  class="container"id="commend1' . $row["id"] . '" >';
                        $i = 0;

                        foreach ($row['listcomment']  as $commend) {
                            if (count($row['listcomment']) - $i < 4) {

                                echo  ' <div class="row">
                                 <div class="col-sm-1 avt-cmt">
                                     <img src="../public/avt.png" alt="avt">
                                     
                                 </div>
                                 <div class="col-sm-8 cmt-content">
                                 <b>
                                        ' . $commend["name"] . '
                                    </b>
                                     <p>
                                         ' . $commend["content"] . '
                                     </p>
                                     <img src="../public/3dot.svg">
                                    
                                     </div>
                 
                                     </div>';
                            }
                            $i++;
                        }
                        echo '    </div>';
                        echo '<div class="hide-cmt">
                        <a  id="box2' . $row["id"] . '"  href="javascript:void(0); id="hide' . $row["id"] . '" onClick="hide111('  . $text . $row["id"] .  $text . ')"style="display:none;">
                        <p style="margin-left: 10px;margin-bottom: 0;font-size: 15px">Ẩn bình luận</p>
                    </a>
                    </div>';
                        echo   '
              
                 
                    
                     <div  class="container"id="commend2' . $row["id"] . '" style="display:none;">';

                        foreach ($row['listcomment']  as $commend) {
                            echo  ' <div class="row">
                             <div class="col-sm-1 avt-cmt">
                                 <img src="../public/avt.png" alt="avt">
                             </div>
                             <div class="col-sm-8 cmt-content">
                             <b>
                                        ' . $commend["name"] . '
                                    </b>
                                 <p>
                                     ' . $commend["content"] . '
                                 </p>
                                 <img src="../public/3dot.svg">
                                
                             </div>
                 
                         </div>';
                        }
                        echo '    </div>';
                    } else {
                        echo   '
                     
                        <div class="comments">
                        
                           
                            <div class="container"id="commend1' . $row["id"] . '">';

                        foreach ($row['listcomment']  as $commend) {
                            echo  ' <div class="row">
                                    <div class="col-sm-1 avt-cmt">
                                        <img src="../public/avt.png" alt="avt">
                                        
                                    </div>
                                    <div class="col-sm-8 cmt-content">
                                    <b>
                                        ' . $commend["name"] . '
                                    </b>
                                        <p>
                                            ' . $commend["content"] . '
                                        </p>
                                        <img src="../public/3dot.svg">
                                       
                                    </div>
                                </div>';
                        }
                        echo '   </div>';
                    }

                    if(isset($_SESSION['email'])){                   
                        echo ' <!-- commnent box -->
    
                            <div class="container cmt-box">
                                <div class="row">
                                    <div class="col-sm-1 avt-cmt">
                                        <img src="../public/avt.png" alt="avt">
                                    </div>
    
                                    <div class="col-sm-9 cmt-content">
                                        <input type="text" class="form-control" name ="content"id="content' . $row["id"] . '" placeholder="Viết bình luận...">
                                        <input hidden=true type="text" class="form-control"id="value' . $row["id"] . '" name ="id_post" id="inputWarning" value="' . $row["id"] . '" placeholder="Viết bình luận...">
    
                                    </div>
                                    <div class="col-sm-1 send">
                                    <button type="submit" onclick="commend('  . $text . $row["id"] .  $text .',' . $text . $_SESSION['name'] .  $text .')" "name="someName" value="someValue"><img src="../public/send.svg"width="40"style="margin:3px; height="30" alt="SomeAlternateText"></button>
    
    
                                    </div>
                                </div>
                            </div>';
                        }
                        echo '</div> 

                </div>';
                }
                ?>
              
    <script>
        var btn = document.getElementById('btn-fl');
        btn.addEventListener('click', function() {
            document.location.href = '<?php echo "follow.php?email=".$email."&temp_email=".$temp_email; ?>';
        });
  </script>
   <script>
                    function getBase64(file) {
                        return new Promise((resolve, reject) => {
                            const reader = new FileReader();
                            reader.readAsDataURL(file);
                            reader.onload = () => resolve(reader.result);
                            reader.onerror = error => reject(error);
                        });
                    }

                    var fileList;
                    var file;

                    function hide111(id) {
                        var x = document.getElementById("commend1" + id);
                        if (x.style.display === "none") {
                            x.style.display = "block";
                        } else {
                            x.style.display = "none";
                        }
                        var y = document.getElementById("commend2" + id);
                        if (y.style.display === "none") {
                            y.style.display = "block";
                        } else {
                            y.style.display = "none";
                        }
                        x = document.getElementById("box1" + id);
                        if (x.style.display === "none") {
                            x.style.display = "block";
                        } else {
                            x.style.display = "none";
                        }
                        x = document.getElementById("box2" + id);
                        if (x.style.display === "none") {
                            x.style.display = "block";
                        } else {
                            x.style.display = "none";
                        }
                    };

                    function show(id) {
                        $("#commend2" + id).hide();
                        $("#commend1" + id).show();

                    };
                    function deletepost(id) {
                        var id_post = $.trim($('#value' + id).val());

                        $.ajax({
                            url: 'delete.php',
                            type: 'POST',
                            data: {
                                'id_post': id_post
                            },
                            success: function(msg) {
                                window.location.reload();

                            }
                        });
                    }
                    function commend(id) {
                        var id_post = $.trim($('#value' + id).val());

                        $.ajax({
                            url: 'like.php',
                            type: 'POST',
                            data: {
                                'id_post': id_post
                            },
                            success: function(msg) {
                            

                            }
                        });
                    }
                    function like(id,count_like) {
                        var count = count_like*1;
                        count += 1; 
                        var id_post = $.trim($('#value' + id).val());

                        $.ajax({
                            url: 'like.php',
                            type: 'POST',
                            data: {
                                'id_post': id_post
                            },
                            success: function(msg) {
                                console.log(msg)
                                if(msg == 0) {
                                    document.getElementById("like" + id).innerHTML = count + " người đã thích bài viết này" ;
                                }
                            }
                        });
                    }
                    function commend(id , name) {
                        var id_post = $.trim($('#value' + id).val());
                        var content = $.trim($("#content" + id).val());

                        $.ajax({
                            url: 'comment.php',
                            type: 'POST',
                            data: {
                                'id_post': id_post,
                                'content': content
                            },
                            success: function(msg) {
                                text = ' <div class="row"><div class="col-sm-1 avt-cmt">' +
                                    ' <img src="../public/avt.png" alt="avt">' +
                                    ' </div>' +
                                    ' <div class="col-sm-8 cmt-content">' +
                                  '  <b>       ' + name + '       </b>'+
                                    '    <p>' +
                                    content +
                                    '    </p>' +
                                    '   <img src="../public/3dot.svg">' +

                                    '  </div>' +
                                    ' </div>';
                                $("#commend1" + id).append(text);
                                $("#commend2" + id).append(text);
                                $('#content' + id).val('');


                            }
                        });
                    }

                    function change(event) {
                        getBase64(event.target.files[0]).then(
                            data => {
                                fileList = data;

                                file = fileList.split('base64,')[1];
                                console.log(file);

                            })
                    }
                    $(document).ready(function() {

                        $('#post_button').click(function() {
                            var title = $.trim($('#title-status').val());
                            var content = $.trim($("#content-status").val());

                            $.ajax({
                                url: 'post.php',
                                type: 'POST',
                                data: {
                                    'content': content,
                                    'title': title,
                                    "file": file
                                },
                                success: function(msg) {
                                    window.location.reload();
                                }
                            });
                        });

                    });
                </script>
</body>

</html>