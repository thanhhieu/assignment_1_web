<?php
    session_start();
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
    if (!isset($_SESSION['email']) && !isset($_SESSION['temp_email'])){
        header("location:../../login/signin-register-forgotpassword/index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/homepage.css" rel="stylesheet">
    <link href="../../homepage-main/css/homepage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/page-user.css">
    <link rel="stylesheet" type="text/css" href="../css/listfriend.css">
    <link rel="stylesheet" type="text/css" href="introduce.css">
    <!-- <script src="../js/homepage.js"></script> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../../homepage-main/public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../homepage-main/html/homepage.php">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                    
                </ul>
            </div>
        </nav>
    </div>


    <div class="banner">
    	<div class="img-cover">
    		
    	</div>
    	<div class="img-user">
    		<div class="avatar">
                <img src="https://khoso.vn/img/avatar.jpg" alt="">    
            </div>
    		<h4><?php echo mysqli_fetch_array($name)['name']; ?></h4>
    	</div>
    </div>	
    
    <div class="nav-user">
    	<ul class="nav nav-tabs">
		  <li class="nav-item">
		    <a class="nav-link" href="user-page.php">Dòng thời gian</a>
		  </li>
		  <li class="nav-item ">
		    <a class="nav-link" href="introduce.php">Giới thiệu</a>
		  </li>
		  <li class="nav-item active">
		    <a class="nav-link" href="#">Bạn bè</a>
		  </li>
		</ul>
    </div>	
    <div class="main-page-friend">
        <div class="friend d-flex justify-content-between">

            <h3>Bạn bè</h3>
        </div>
        <hr>
        <!-- <div class="container-fluid">
            <div class="row">
                <div class="list-fr col-md-6">

                </div>
                <div class="list-fr col-md-6">
                    
                </div>
            </div>
        </div> -->
        <div class="main-top">
            <div class="title">
                <h5>Tất cả bạn bè</h5>
            </div>
            <div class="search d-xs-block">
                <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm bạn bè" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div>
        </div>
        <div class="container-fluid">
            <div class="items row">
                <?php
                    $temp_email = $_SESSION['temp_email'];
                    $name =mysqli_query($con,"select * from user where email= '$temp_email'");
                    $id_user_temp = mysqli_fetch_array($name)['id'];
                    // echo mysqli_fetch_array($name)['id'];
                    $query_fr =mysqli_query($con,"select * from user_friend where id_user= '$id_user_temp'");
                    // $listfr = mysqli_fetch_array($query_fr);
                    // echo $listfr['id_friend']
                    while ($row = mysqli_fetch_array($query_fr)) {
                        $id_fr = $row{'id_friend'};
                        // echo $id_fr;
                        $find_fr = mysqli_query($con,"select * from user where id = '$id_fr'");
                        $name_fr = mysqli_fetch_array($find_fr)['name'];
                    // }
                ?>
                <div class="item col-12 col-lg-6 col-md-12 col-sm-12">
                    <div class="main-item">
                        <div class="image">
                            <img src="https://khoso.vn/img/avatar.jpg" alt=""> 
                        </div>
                        <div class="name">
                            <!-- <h5>Sang Nguyen</h5> -->
                            <a href="<?php echo "../../database/profile_user.php?id=". $id_fr; ?>"><?php echo '<h5>'. $name_fr.'</h5>'; ?></a>
                            
                        </div>
                        <div class="button">
                            <button class="btn">Bạn bè</button>
                        </div>  
                    </div>
                </div> 
                <?php
                    }
                ?>   
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <p>&copy; Nhóm lập trình web số 4</p>
            </div>
        </div>
    </div>
</body>

</html>