<?php
    session_start();
    include("../change_intro.php");
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
    if (!isset($_SESSION['email']) && !isset($_SESSION['temp_email'])){
        header("location:../../login/signin-register-forgotpassword/index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../css/homepage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/page-user.css">
    <link rel="stylesheet" type="text/css" href="../introduce.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                   <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../../../homepage-main/public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../../homepage-main/html/homepage.php">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </div>

    <!-- <div class="menu">
        <ul>
            <li><a href="#">BẢNG TIN</a></li>
            <li><a href="#">TRẢI NGHIỆM</a></li>
            <li><a href="#">LỌC</a></li>
        </ul>
    </div> -->

    <div class="banner">
        <div class="img-cover">
            
        </div>
        <div class="img-user">
            <div class="avatar">
                <img src="" alt="">    
            </div>
            <h4>Nguyen Sang</h4>
        </div>
    </div>  
    
    <div class="nav-user">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link" href="../introduce.php">Dòng thời gian</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">Giới thiệu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../listfriend.php">Bạn bè</a>
          </li>
        </ul>
    </div>  
        <div class="main-page-intro">
            <h3>Giới thiệu</h3>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-left col-md-2">
                        <div class="nav">
                            <a class="nav-link" href="../introduce.php">Thông tin cơ bản</a>
                            <a class="nav-link active">Chi tiết</a>
                        </div>
                    </div>
                    <?php
                        $con = mysqli_connect("localhost","root","","travelsocial1");
                        $email = $_SESSION['temp_email'];
                        $query_intro =mysqli_query($con,"select * from user where email='$email'");
                        $intro = mysqli_fetch_array($query_intro);
                        $name = $intro['name'];
                        $detail = $intro['detail'];
                    ?>    
                    <div class="col-main col-md-7">
                        <h3>Giới thiệu bản thân</h3>
                        <hr>
                        <form method="post">
                          <div class="form-group row">
                            <label  class="col-sm-2 col-form-label">Tên</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control h-100" placeholder="Tên" name="name" value=<?php echo $name; ?>>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label  class="col-sm-2 col-form-label">Giới thiệu</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" rows="3" placeholder="Giới thiệu bản thân" name="detail" ><?php echo $detail; ?></textarea>
                            </div>
                          </div>
                            <?php
                            if(isset($_SESSION['email'])){
                                if($_SESSION['temp_email'] == $_SESSION['email']){
                            ?>
                            <div class="text-right">
                                <button type="submit button" class="btn btn-success" name="save-detail">Lưu thay đổi</button>
                                <button type="button" class="btn btn-secondary">Hủy</button>
                            </div>
                            <?php
                                }}
                            ?>
                        </form>
                        <div class="main-footer">
                        </div>
                    </div>

                    <div class="col-right col-md-3">
                    </div>
                </div>
            </div>
    </div>
</body>

</html>