<?php
    session_start();
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/nhahatTP.css" rel="stylesheet">
    <script src="../js/homepage.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../../homepage-main/public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../homepage-main/html/homepage.php">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </div>

    <div class="menu">
        <ul>
            <li><a href="../../homepage-main/html/homepage.php">BẢNG TIN</a></li>
            <li><a href="explore.php">TRẢI NGHIỆM</a></li>
        </ul>
    </div>

    <div class="main-page">
        <div class="banner bg-banner">
            <div class="banner-text">
                <h1 style="font-size: 6vw;">Nhà hát lớn thành phố</h1>
                <p style="font-size: 1.5vw;">Nhà Hát Giao Hưởng – Nhạc, Vũ Kịch Thành Phố Hồ Chí Minh còn gọi là Nhà Hát
                    Lớn Thành Phố, hay thường
                    được biết đến với tên gọi Nhà hát Thành Phố, nằm trên đường Công trường Lam Sơn, Quận 1, Thành phố
                    Hồ Chí Minh. Nằm ở vị trí thuận lợi tại trung tâm thành phố, nhà hát được xem là nhà hát trung tâm,
                    đa năng chuyên tổ chức biểu diễn sân khấu nghệ thuật đồng thời cũng được sử dụng để tổ chức một số
                    sự kiện lớn.</p>
            </div>
        </div>

        <div class="content">
            <p>Nhà hát Thành phố là nhà hát thuộc loại lâu đời theo kiến trúc Tây Âu và được xem như một địa điểm du
                lịch của thành phố Hồ Chí Minh</p>
            <div class="location-image">
                <img src="../public/explore/nhahat1.jpg" alt="" width="800" height="400">
            </div>
            <p>Nằm ở điểm cuối đường Lê Lợi, tọa lạc trên con đường Ðồng Khởi – trung tâm thành phố Hồ Chí Minh, bên
                cạnh là hai khách sạn lớn Caravelle và Continental. Nhà hát được xem như một công trình văn hóa tiêu
                biểu và tốn kém nhất ở Sài Gòn thời Pháp thuộc.</p>
            <div class="location-image">
                <img src="../public/explore/nhahat2.jpg" alt="" width="800" height="400">
            </div>
            <p>
                Hiện nay Nhà hát thành phố Hồ Chí Minh là nơi tổ chức biểu diễn sân khấu chuyên nghiệp như: biểu diễn
                kịch nói, cải lương, ca nhạc, múa ba lê, dân tộc, opera cho tất cả các đoàn nghệ thuật trong và ngoài
                nước
            </p>
            <p>Nhà hát được khởi công và đến ngày 1 tháng 1 năm 1900 thì khánh thành. Năm 1998, nhân dịp 300 năm khai
                sinh Sài Gòn , Nhà hát Lớn thành phố Hồ Chí Minh được tu bổ bảo tồn phong cách kiến trúc ban đầu, với
                một số trang trí, điêu khắc nổi ở mặt tiền như tượng nữ thần nghệ thuật, các dây hoa… được phục chế,
                đồng thời trang bị hệ thống chiếu sáng mỹ thuật về đêm, đến năm 2009 thì hoàn thành.</p>
            <div class="location-image">
                <img src="../public/explore/nhahat3.jpg" alt="" width="800" height="400">
            </div>
            <p>Nằm trên đường Đồng Khởi, nơi đây vừa là nhà hát, vừa là nơi thường xuyên tổ chức nhiều chương trình ca
                nhạc nghệ thuật. Về đêm, người dân Sài Gòn thường tụ tập phía trước bậc thang, vừa ngồi nhìn dòng người
                tấp nập chạy trên đường, vừa nhâm nhi ly trà sữa phía sau nhà hát.</p>
        </div>
    </div>

</body>

</html>