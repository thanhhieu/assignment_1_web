<?php
    session_start();
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/hochiminh.css" rel="stylesheet">
    <script src="../js/homepage.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../../homepage-main/public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../homepage-main/html/homepage.html">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </div>

    <div class="menu">
        <ul>
            <li><a href="../../homepage-main/html/homepage.php">BẢNG TIN</a></li>
            <li><a href="explore.php">TRẢI NGHIỆM</a></li>
        </ul>
    </div>

    <div class="main-page">
        <div class="banner bg-banner">
            <div class="banner-text">
                <h1>HỒ CHÍ MINH</h1>
                <p>Thành phố Hồ Chí Minh, thường được gọi bằng cái tên thân thuộc Sài Gòn. Hòn ngọc viễn đông xưa kia đã
                    phát triển hơn rất nhiều. Vẫn giữ lại những nét văn hóa vùng miền rất riêng của mình, thành phố này
                    hiện tại còn là thủ phủ thương mại của Việt Nam. Bạn có thể tìm thấy một cuộc sống hối hả ngày đêm
                    ngay cạnh những công trình kiến trúc thời thực dân Pháp; thưởng thức đủ mùi đủ vị "Sài Gòn" tại lề
                    đường hoặc những quán ăn sang trọng. Thành phố Hồ Chí Minh cũng hứa hẹn nhiều câu chuyện thú vị qua
                    những bảo tàng, di tích còn sót lại của Gia Định xưa kia...</p>
            </div>
        </div>

        <div class="block-items">
            <h3>Điểm đến nổi bật</h3>
            <div class="child-page-listing">
                <div class="grid-container">
                    <a href="../html/NhahatTP.php">
                        <div class="polaroid">
                            <img src="../public/explore/nhathatlon-card.jpg" alt="5 Terre" style="width:100%">
                            <div class="container">
                                <p class="title">Nhà hát lớn thành phố</p>
                                <p class="ad">7 Công trường Lam Sơn, quận 1</p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="polaroid">
                            <img src="../public/explore/HCM.jpg" alt="Norther Lights" style="width:100%">
                            <div class="container">
                                <p class="title">Nhà thờ chánh tòa Đức bà</p>
                                <p class="ad">Công xã Paris, Bến Nghé, quận 1</p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="polaroid">
                            <img src="../public/explore/dinhdoclap.jpg" alt="Norther Lights" style="width:100%">
                            <div class="container">
                                <p class="title">Dinh Độc Lập</p>
                                <p class="ad">135 Nam Kì Khởi Nghĩa, Bến Thành, quận 1</p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="polaroid">
                            <img src="../public/explore/chobenthanh.jpg" alt="Norther Lights" style="width:100%">
                            <div class="container">
                                <p class="title">Chợ Bến Thành</p>
                                <p class="ad">Phan Bội Châu, Bến Thành, quận 1</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
</div>
</body>

</html>