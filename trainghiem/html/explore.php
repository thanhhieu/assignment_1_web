<?php
    session_start();
    $con = mysqli_connect("localhost","root","","travelsocial1");
    if (isset($_SESSION['email'])){
        $email = $_SESSION['email'];
        $name_main =mysqli_query($con,"select * from user where email= '$email'");
    }
    if (isset($_SESSION['temp_email'])){
        $temp_email = $_SESSION['temp_email'];
        $name =mysqli_query($con,"select * from user where email= '$temp_email'");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trải nghiệm</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/explore.css" rel="stylesheet">
    <script src="../js/homepage.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2ecc5b7742.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="header">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="../../homepage-main/html/homepage.php"><img src="../../homepage-main/public/logo.jpg" alt="Logo"
                    style="width:60px;height:30px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." aria-label="Search">
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="../../homepage-main/public/avt.png" style="height: 32px;width:32px;">
                            <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#" style="padding-left: 0;"> -->
                        <a class="nav-link" href="<?php echo "../../database/profile_user.php?id=". $_SESSION['id_user']; ?>">
                            <b>
                                <?php 
                                        echo mysqli_fetch_array($name_main)['name']; 
                                ?></b> <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        }
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../../homepage-main/html/homepage.php">Trang chủ<span class="sr-only">(current)</span></a>
                    </li>
                    <?php 
                        if (isset($_SESSION['email'])){
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../database/logout.php">Logout</a></button>
                            </li>
                    <?php 
                        }else{
                    ?>
                            <li>
                                <button type="button" class="btn btn-light"><a href="../../login/signin-register-forgotpassword/index.php">Login</a></button>
                            </li>
                    <?php 
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </div>

    <div class="menu">
        <ul>
            <li><a href="../../homepage-main/html/homepage.php">BẢNG TIN</a></li>
            <li><a href="#">TRẢI NGHIỆM</a></li>
        </ul>
    </div>

    <div class="main-page">
        <div class="title">
            <h3>Điểm đến nổi bật</h3>
            <p>Khám phá điểm đến tiếp theo cho hành trình tuyệt vời của bạn</p>
        </div>
        <div class="child-page-listing">
            <div class="grid-container">
                <article id ="1" class="location-listing">
                    <a class="location-title" href="../html/hochiminh.php">
                        <h2>Hồ Chí Minh</h2> </a>
                    <div class="location-image">
                        <a href="../html/hochiminh.php">
                            <img width="400" height="300"
                                src="../public/explore/HCM.jpg"
                                alt="HCM"> </a>
                    </div>
                </article>
                <article id ="2" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Lâm Đồng</h2></a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300"
                                src="../public/explore/lamdong.jpg"
                                alt="Lam Dong"> </a>
                    </div>
                </article>
                <article id ="3" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Hội An</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300"
                                src="../public/explore/hoian.jpg"
                                alt="Hoi An"> </a>
                    </div>
                </article>
                <article id ="4" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Đà Nẵng</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300"
                                src="../public/explore/danang.jpg"
                                alt="Da Nang"> </a>
                    </div>
                </article>
                <article id ="5" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Quảng Ninh</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300"
                                src="../public/explore/quangninh.jpg"
                                alt="Quang Ninh"> </a>
                    </div>
                </article>
                <article id ="6" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Lào Cai</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300" alt="Lao Cai"
                                src="../public/explore/laocai.jpg">
                        </a>
                    </div>
                </article>
                <article id ="7" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Huế</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300" alt="Hue"
                                src="../public/explore/hue.jpg">
                        </a>
                    </div>
                </article>
                <article id ="8" class="location-listing">
                    <a class="location-title" href="#">
                        <h2>Phú Quốc</h2> </a>
                    <div class="location-image">
                        <a href="#">
                            <img width="400" height="300" alt="Phu Quoc"
                                src="../public/explore/phuquoc.jpg">
                        </a>
                    </div>
                </article>
            </div>
            <!-- end grid container -->
        </div>
    </div>
</body>

</html>